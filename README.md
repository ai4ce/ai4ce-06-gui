# AI4CE Repo Template

## Setup
To start out you only need to suffice the following requirements:
1. Have Python 3.11 installed and set as your default Python
2. Having Poetry installed, which can be achieved over pip with the command 'pip install poetry'


Initial setup with poetry
For the initial setup, it is sufficient to simply run the two commands below.
Poetry shell will start an instance of a virtual environment of poetry.
Poetry install will install and/or modify the existing environment, taking care of dependencies for you.
```
poetry install
poetry shell
```

## Development Routine
### Run your code
To run your code, you need to execute it in the virtual environment of poetry. There are at least 2 ways to achieve this.
The most straightforward way is to run it on your console as normal, simply attaching 'poetry run' as a prefix.
Example:
```
poetry run python your_project_name/main.py
```
The other option is to set the installed poetry venv in your IDE. How this is done depends on your IDE, but you would usually have to find the path to the location where the environment is installed to and set it as a location for a Python runtime environment in your IDE

### Test your code
To test your code you can use pytest, which comes installed by the poetry environment. You should write tests extensively. An example test can be found in the dedicated tests folder and can be run like this:
```
poetry run pytest
```
For more arguments on how to run tests you can refer to the pytest documentation.

For more comprehensive testing, Hypothesis has been added to the testing environment. Hypothesis allows for so-called property based testing by comfortably letting you test every conceivable input and discover edge cases for inputs. The use of Hypothesis is therefore highly encouraged to ensure a better code coverage of your tests!
An example of how a Hypothesis test can look like is found in the tests class, but you should refer to the official documentation for further details: https://hypothesis.readthedocs.io/en/latest/index.html
Hypothesis runs piggy-back on any existing testing-tool and thus will be run together with pytest when invoking the command above!

### Check your changes for style- and convention-compliance
To ensure a common style in code writing for readability and thus reusability, we employ pre-commit, which is a tool that can host various sub-tools (so called hooks) to check not only your code but also documentation for adherence to coding conventions and can to some degree automatically fix errors. You can run pre-commit hooks like this:
```
pre-commit run -a
```

### Commit and push your changes
If everything checks out in the tests and pre-commit, then your code may be ready for pushing. This is done by the three steps:
1. Add
2. Commit
3. Push
```
git add <changes>
git commit -m "<message>"
git push
```
The last step may need resolving, like merging files.


### Generate code documentation
This template also provides you with a powerful tool to generate code documentation: Sphinx
Sphinx can convert your entire project into an easily comprehensible documentation of that project in a few selected formats.
The most prominent one is as html files, but pdf are also an option. This tutorial will focus on how to generate pdf. For more info about sphinx, please check out the official documentation: https://www.sphinx-doc.org

Most things are already setup to automatically generate a documentation for your entire project, but a few changes should be made before creating your own:
In the docs folder the file conf.py contains the configuration. Here you should change the author and project name and can freely adjust the version number. This is also the place to add extensions or change the theme of the html, but we recommend keeping the theme as to retain a project over-arching uniformity.
After that you only need to execute two commands to create the documentation for each module that has an __init__.py file (thus make sure whether you have own for every module to be documented)!
```
sphinx-apidoc -o docs .
cd docs
make html
cd ..
```
It may also be necessary to install the functionality to execute the make command, which is operating-system specific. If everything went well, then you should be able to find the corresponding html files inside the _build folder. Now you simply have to open them in a browser of your choice!

## Building a Docker image
### Basics
Docker containers are built using a Dockerfile in the root of your project. It contains instructions on what to include/install/do in the container. Every container image contains a base image as a starting point. The Python project provides base images that we can use. I'm using the Alpine version of the python image, since it is extremely lightweight. Alpine is a very lightweight distro.
The jist of building a docker container can be summed up as follows:

1. select a base image
2. copy project into container
3. get/install dependencies (with poetry)
4. specify command to run when the container is launched (this is the command which starts your program inside the container)

### Commands explained

You'll see the commands for a Dockerfile are pretty self-explanatory:
`FROM` base image
`RUN` command
`COPY` folder/file-on-host folder/file-in-container (source destination)
`WORKDIR` translates to the `cd` aka change directory command
`ENTRYPOINT` program to be run
`CMD` list of arguments passed to the ENTRYPOINT program

Additional commands are
`ENV` specify environment variables (will be overwritten by variables specified during deployment)
`USER` to run the program as (you'll have to create the user first)

### The template Dockerfile

Things to note about the Dockerfile:

1. It's a two staged build, as you can see by the two `FROM` commands. Since we don't need poetry to actually run our program, we don't want it installed in our final container.
    - So our first stage will install poetry, copy the pyproject.toml into the container so that poetry can create the requirements.txt
    - We then start a new container image with a new base image (also python-alpine), import the requirements.txt generated by the first stage, install only the requirements for running our program and do the rest
2. Creating a non-root user. This is just best practice

## Install Docker
To test your Dockerfile, you'll obviously need docker installed.

- For Windows there is Docker Desktop, which utilizes WSL2 by default, but can also run with Hyper-V (both are virtualization technologies)
Note: I've had issues with WSL performance on AMD chips, so i switched to Hyper-V on my Laptop (still WSL on Desktop/Intel)
- For linux depending on your distro and package manager, e.g. ubuntu: `sudo apt install -y docker docker-compose`

## Building the docker image
In the root of your project (where your Dockerfile is located) run:
`docker build . -t myprojectname:versiontag`
`-t` specifies the tag/name + version of the container image. It's all arbitrary. When using CI/CD you should always create two container images

1. `-t myprojectname:currentversion`
2. `-t myprojectname:latest`

That way when deploying you can just specify :latest as the image tag and always have the most recent version of the image deployed.

## Deploying the docker container
After a successful build the docker program will output the container name and ID. You can now run the container:
```
docker run myprojectname:versiontag
```